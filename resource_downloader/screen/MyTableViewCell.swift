import UIKit

final class MyViewCell: UITableViewCell{
    
    let activityView = UIActivityIndicatorView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildSubviews()
        buildConstraints()
        styleIdle()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        styleIdle()
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("dealloc \(self)")
    }
    
    private func buildSubviews(){
        contentView.addSubview(activityView)
    }
    
    private func buildConstraints(){
        activityView.translatesAutoresizingMaskIntoConstraints = false
        // Dynamic height - Constraint the content view
        NSLayoutConstraint.activate([
            activityView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            activityView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        ])
    }
    
    func styleIdle() {
        activityView.stopAnimating()
        imageView?.tintColor = .systemGray
        accessoryType = .none
        backgroundColor = .white
        imageView?.image = UIImage(systemName: "music.note")
    }
    
    func styleDownloading() {
        activityView.startAnimating()
        imageView?.tintColor = .systemGray
        accessoryType = .none
    }
    
    func styleDownloaded() {
        activityView.stopAnimating()
        imageView?.tintColor = .systemGreen
        accessoryType = .disclosureIndicator
    }
    
    func styleError() {
        activityView.stopAnimating()
        imageView?.tintColor = .systemRed
        accessoryType = .none
    }
}

final class MyTableViewController: UIViewController{

    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: .grouped)
        tableView.register(MyViewCell.self, forCellReuseIdentifier: "cellid")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    //MARK: - Lifecycle

    deinit{
        print("dealloc \(self)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        buildStyle()
        buildConstraints()
    }
    
    private func buildStyle(){
    }
    
    private func buildConstraints(){
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            tableView.widthAnchor.constraint(equalTo: view.widthAnchor),
            tableView.heightAnchor.constraint(equalTo: view.heightAnchor),
        ])
    }
}

extension MyTableViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! MyViewCell
        cell.backgroundColor = .lightGray
        return cell
    }
}

extension MyTableViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       print("Table view did select row at \(indexPath)")
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let contentView = UIView()
        contentView.backgroundColor = UIColor.white
        let label = UILabel()
        label.text = "Section \(section)"
        label.font = UIFont.boldSystemFont(ofSize: 40)
        label.textColor = UIColor.black
        
        // Build constraints
        contentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.topAnchor),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            label.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 5),
            label.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -5),
        ])
        return contentView
    }
}

#if DEBUG && canImport(SwiftUI) && canImport(Combine)
import SwiftUI

@available(iOS 13.0, *)
struct MyViewCellRepresentable: UIViewRepresentable {
  func makeUIView(context: Context) -> UIView {
    let cell = MyViewCell()
    cell.backgroundColor = .lightGray
    cell.textLabel?.text = "Text label"
    cell.detailTextLabel?.text = "Detail text label"
    cell.imageView?.image = UIImage(systemName: "sun.min.fill")
    cell.imageView?.tintColor = .yellow
    cell.accessoryType = .disclosureIndicator
    return cell
  }
  
  func updateUIView(_ view: UIView, context: Context) {
  }
}

struct MyTableViewControllerRepresentable: UIViewRepresentable {
  func makeUIView(context: Context) -> UIView {
    let vc = MyTableViewController()
    return vc.view
  }
  
  func updateUIView(_ view: UIView, context: Context) {
  }
}

struct MyViewCellPreview: PreviewProvider {
  static var previews: some View {
    MyViewCellRepresentable().previewLayout(.fixed(width: 400, height: 60))
    MyTableViewControllerRepresentable().previewDevice(PreviewDevice(rawValue: "iPhone SE (2nd generation)"))
  }
}
#endif
