import UIKit

final class ListViewController: UIViewController {

    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: .grouped)
        tableView.register(MyViewCell.self, forCellReuseIdentifier: "cellid")
        tableView.rowHeight = 65.0
        tableView.dataSource = self
        tableView.delegate = self
        return tableView
    }()
    
    private var adapterList = [UITableViewCell: ReusableCellResourceAdapter]()
    
    private func getReusableAdapter(forCell cell: UITableViewCell) -> ReusableCellResourceAdapter{
        if let adapter = adapterList[cell]{
            //print("Find a reusable adapter")
            return adapter
        }
        else{
            //print("Create a reusable adapter")
            let adapter = ReusableCellResourceAdapter()
            adapterList[cell] = adapter
            return adapter
        }
    }
    
    //MARK: - Lifecycle

    deinit{
        print("dealloc \(self)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        ReusableCellResourceAdapter().deleteDestinationDirectory()
        view.addSubview(tableView)
        buildStyle()
        buildConstraints()
    }
    
    private func buildStyle() {
    }
    
    private func buildConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            tableView.widthAnchor.constraint(equalTo: view.widthAnchor),
            tableView.heightAnchor.constraint(equalTo: view.heightAnchor),
        ])
    }
}

extension ListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! MyViewCell
        
        let filename = "\(indexPath.row).m4a"
        let baseUrl = "https://bitbucket.org/manu2/data/raw/master/sound/"
        let path = baseUrl + filename
   
        let adapter = getReusableAdapter(forCell: cell)
        
        cell.textLabel?.text = "Downloading \(filename)"
        cell.styleDownloading()
        adapter.configure(forResourcePath: path) { data in
            
            if let _ = data {
                cell.textLabel?.text = "Downloaded \(filename)"
                cell.styleDownloaded()
            }
            else{
                cell.textLabel?.text = "Error with \(filename)"
                cell.styleError()
            }
        }
        return cell
    }
}

extension ListViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       print("Table view did select row at \(indexPath)")
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let contentView = UIView()
        contentView.backgroundColor = UIColor.white
        let label = UILabel()
        label.text = "Section \(section)"
        label.font = UIFont.boldSystemFont(ofSize: 40)
        label.textColor = UIColor.black
        
        // Build constraints
        contentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.topAnchor),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            label.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 5),
            label.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -5),
        ])
        return contentView
    }
}

#if DEBUG && canImport(SwiftUI) && canImport(Combine)
import SwiftUI

@available(iOS 13.0, *)
struct ListViewControllerRepresentable: UIViewRepresentable {
  func makeUIView(context: Context) -> UIView {
    let vc = ListViewController()
    return vc.view
  }
  
  func updateUIView(_ view: UIView, context: Context) {
  }
}

@available(iOS 13.0, *)
struct ListViewControllerPreview: PreviewProvider {
  static var previews: some View {
    ListViewControllerRepresentable().previewDevice(PreviewDevice(rawValue: "iPhone SE (2nd generation)"))
  }
}
#endif
