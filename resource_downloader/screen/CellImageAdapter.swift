import UIKit

/// The goal of ReusableCellResourceAdapter object is to :
/// - Download resource from the network
/// - Network error detection
/// - Call the completion handler only for the last data task
/// - Be independant from UICellView lifecycle
/// - Cells data must be updated by datasource
/// - Save the resource on filesystem
final class ReusableCellResourceAdapter {
    var latestTaskId: String = ""
    var tasks = [URLSessionDataTask]()
    
    private static let sessionConfiguration: URLSessionConfiguration = {
        let configuration = URLSessionConfiguration.default
        // For each task, ignore the session cache, and load image from server
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        // For each task, load image from cache, otherwise load from server
        //configuration.requestCachePolicy = .returnCacheDataElseLoad
        return configuration
    }()
    
    private static let session: URLSession = {
        let session =  URLSession(configuration: sessionConfiguration)
        return session
    }()
    
    static func cancelAllTasks() {
        session.invalidateAndCancel()
    }
    
    //MARK: - FILESYSTEM
    
    /// Application document path.
    ///
    /// - Returns: path
    private func docDirectoryPath() -> String{
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                           .userDomainMask,
                                                           true)
        return dirPaths[0]
    }
    
    private func destinationUrl(forRequestUrl requestUrl: URL) -> URL {
        var url = destinationDirectory()
        url.appendPathComponent(requestUrl.lastPathComponent)
        return url
    }
    
    private func destinationDirectory() -> URL {
        var url = URL(fileURLWithPath: docDirectoryPath())
        url.appendPathComponent("sound/")
        try? FileManager.default.createDirectory(
                    at: url,
                    withIntermediateDirectories: false,
                    attributes: nil)
        return url
    }
    
    func deleteDestinationDirectory() {
        let url = destinationDirectory()
        try? FileManager.default.removeItem(at: url)
    }
    
    private func destinationUrlExist(forRequestUrl url: URL) -> Bool {
        let destinationUrl = destinationUrl(forRequestUrl: url)
        return FileManager.default.fileExists(atPath: destinationUrl.path)
    }
    
    private func getDataFromLocal(forRequestUrl url: URL) -> Data? {
        let destinationUrl = destinationUrl(forRequestUrl: url)
        return try? Data(contentsOf: destinationUrl)
    }
    
    //MARK: - Data tasks
    
    /// Configure and cancel old requests.
    /// This method is called from several threads (as mush as reusable cell count)
    /// There is a dependency to UIKit, but it is not dependant of UIView lifecycle.
    /// It takes the responsability to unwrap uiimage.
    func configure(forResourcePath path: String, completionHandler: @escaping ((Data?) -> ()) ) {
        guard let url = URL(string: path) else { return }
        
        if(destinationUrlExist(forRequestUrl: url)) {
            let data = getDataFromLocal(forRequestUrl: url)
            completionHandler(data)
            return
        }
        
        // Keep an history of the request.
        latestTaskId = UUID().uuidString
        let checkTaskId = latestTaskId
        
        
        // Download the image asynchronously
        let latestTask = Self.session.dataTask(with: url) {[weak self] (data, response, error) in
            guard let weakSelf = self else {return}
            //Debug by increasing lag (add 50ms)
            //usleep(50_000)
            
            if let err = error {
                DispatchQueue.main.async {
                    if(weakSelf.latestTaskId == checkTaskId) {
                        completionHandler(nil)
                    }
                }
                print(err)
                return
            }
            // Return the data, only if the taskId match, and if
            DispatchQueue.main.async {
                if let response = response as? HTTPURLResponse,
                   response.statusCode == 200 {
                    if let data = data{
                        let destinationUrl = weakSelf.destinationUrl(forRequestUrl: url)
                        print("Filesystem : Write file \(destinationUrl.lastPathComponent)")
                        try? data.write(to: destinationUrl)
                        if(weakSelf.latestTaskId == checkTaskId) {
                            completionHandler(data)
                        }
                    }
                    else {
                        if(weakSelf.latestTaskId == checkTaskId) {
                            completionHandler(nil)
                        }
                    }
                }
                else{
                    if(weakSelf.latestTaskId == checkTaskId) {
                        completionHandler(nil)
                    }
                }
            }
        }
        latestTask.resume()
        tasks += [latestTask]
    }    
}
